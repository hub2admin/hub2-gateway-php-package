#Hub2 Gateway PHP SDK
`HUB2` est une plateforme qui propose plusieurs moyens de paiement, pour les e-commercants partenaires, tels que:
- `Mobile Banking`
- `Carte Bancaire`
- `Airtime`

##Documentation
Hub2 Gateway - PHP SDK [suivre le lien](https://download.hub2.io/docs/package-php-hub2gateway-documentation.pdf)

##Vulnérabilités en matière de sécurité
Si vous découvrez une faille de sécurité dans Hub2 Gateway, veuillez envoyer un e-mail à Hub2 via [technology@hub2.io](mailto:technology@hub2.io). Toutes les vulnérabilités en matière de sécurité seront traitées rapidement.

##Licence d'utilisation
Hub2 Gateway PHP SDK est un logiciel open-source sous [licence MIT](http://opensource.org/licenses/MIT).