# Documentation

Cette documentation fournit une introduction rapide sur `Hub2Gateway` et des exemples introductifs.

## Prérequis

Pour l'utilisation du package vous aurez besoin au minimum
- `Composer`
- `PHP7` ou plus

## Comment installer

La méthode recommandée pour installer ce package est **Composer**. Composer est un outil de gestion des dépendances pour PHP qui vous permet de déclarer les dépendances dont votre projet aura besoin et de les installer au sein de votre projet.
```BASH
# Install Composer
curl -sS https://getcomposer.org/installer | php
```

Pour installer le package `Hub2Gateway` en utilisant composer
```BASH
$ composer require hub2/hub2-gateway
```

Vous pouvez également spécifier **Hub2Gateway** en tant que dépendance dans le fichier `composer.json` existant au sein de votre projet:
```JSON
{ 
   "require" :  { 
      "hub2/hub2-gateway" :  "~0.5" 
   } 
}
```

Après l'installation, vous aurez besoin de l'autoloader de Composer:
```PHP
require 'vendor/autoload.php';
```

## Comment l'utiliser

L'utilisation du package est tout simple, il comprend des méthodes static vous permettant de faciliter l'intégration.

#### Configuration

Pour utiliser le service de payment `Hub2` vous avez besoin de définir le `merchnat_id` et l'`api_key`. Le package Hub2Gateway a une méthode facilitant l'initialisation de ces informations.
```PHP
Hub2Gateway::setConfig({
    "merchant_id": 1,
    "api_key": "your api key" 
})
```

Si vous êtes en production vous pouvez préciser cela en définissant le paramètre `sanbox` à `false`

```PHP
Hub2Gateway::setConfig({
    "merchant_id": 1,
    "api_key": "your api key" ,
    "sandbox": false
});
```

#### Filtrer le contenu

Imaginer que vous avez un tableau contenant plusieurs données et vous ne souhaitez pas faire des manipulations pour retourner seulement les champs requis pour l'ouverture d'une transaction `Hub2`. Cette méthode vous aide dans ce genre de cas. Prenons l'exemple ci-dessous.

```php
$data = [
    'shop' => 'Demo Shop',
    'amount' => 20,
    'customer_id' => '1',
    'currency' => 'XOF',
    'method' => 'mb',
    'purchase_ref' => 'DEMO234590L',
    'country' => 'FR',
]
``` 

Maintenant faisons appel à notre méthode magique
```php
...

$postData = Hub2Gateway::fillContent($data)
``` 

Voyons le contenu de `$postData`
```php
...

$postData = Hub2Gateway::fillContent($data);

print_r($postData);

// $data = [
//    'amount' => 20,
//    'customer_id' => '1',
//    'currency' => 'XOF',
//    'method' => 'mb',
//    'purchase_ref' => 'DEMO234590L',
// ]
``` 

#### Preparer une transaction

Préparer une transaction revient à mettre les données au bon format pour l'ouverture d'une transaction
```php
$data = [
    'amount' => 20,
    'customer_id' => '1',
    'currency' => 'XOF',
    'method' => 'mb',
    'purchase_ref' => 'DEMO234590L',
];

$requestContent = Hub2Gateway::prepareTransaction($data);

print_r($requestContent);

//[
//    'customer_id' => 1,
//    'in' => [
//        'amount' => 20,
//        'method' => 'mb',
//        'currency' => 'XOF',
//    ],
//    'purchase_ref' => 'DEMO234590L',
//]
``` 
#### Affichage modal

Pour afficher la `Modal` hub2 vous aurez besoin des fichiers `css` et `js` de `Hub2`. La classe `Hub2Gateway` a en effet deux méthodes pour faciliter l'intégration. Ces méthodes prennent en paramètre le `token` et `tuid`.
```PHP
# Pour le CSS
Hub2Gateway::cssInit($token, $tuid)

# Pour le JS
Hub2Gateway::scriptInit($token, $tuid)
```
Pour que cela puisse mieux fonctionner vous aurez besoin des dépendances JS supplémentaire.

| Name  | Type | HTML intégration |
|-------|------|------------------|
| Select2  | **CSS** | `<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css">`  |
| jQuery | **JS** | `<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>` |
| jQuery redirect | **JS** | `<script src="https://js.hub2.io/external/jquery-redirect.js"></script>` |
| Select2 | **JS** | `<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>` |

#### Ouverture d'une transaction
L'ouverture d'une transaction se fait par simple appel grâce à la méthode `openTransaction`

```PHP
Hub2Gateway::openTransaction();
```

Cette méthode retourne une réponse contenant le `tuid` et le `token`. Ces informations sont nécessaires pour l'ouverture du `modal`. Il est également possible d'écouter les exceptions en cas d'erreur.

```PHP
try {
   Hub2Gateway::openTransaction();
} catch (\Hub2\Exceptions\Hub2Exception $e) {
    //
}
```

#### Verifier le statut d'une transaction

La vérification d'une transaction se fait à l'aide du `tuid` obtenu lors de l'ouverture d'une transaction
``` PHP
Hub2Gateway::checkTransaction($tuid);
```

Cette méthode retourne une réponse contenant le `statut` de la transaction. Si vous souhaitez écouter les exceptions il est possible de le faire également avec cette méthode.
```PHP
try {
   Hub2Gateway::checkTransaction($tuid);
} catch (\Hub2\Exceptions\Hub2Exception $e) {
    //
}
```

#### Annuler une transaction

Lors de l'annulation d'une transaction `Hub2` l'API fait un appel `POST` vers votre `URL` d'annulation, ce package intègre une méthode simple pour changer le statut d'une transaction seulement en passant en paramètre le `tuid` de la transaction.
``` PHP
Hub2Gateway::cancelTransaction($tuid);
```

Cette méthode retourne une réponse pour élément `status` et `success`. Si vous souhaitez écouter les exceptions
```PHP
try {
   Hub2Gateway::cancelTransactionn($tuid);
} catch (\Hub2\Exceptions\Hub2Exception $e) {
    //
}
```
