<?php

namespace Hub2;

use Hub2\Abstracts\Hub2Abstract;
use Hub2\Helpers\Hub2RequestHelper;
use Hub2\Helpers\Hub2ValidatorHelper;
use Hub2\Traits\Hub2GatewayHelperTrait;
use Hub2\Traits\Hub2GetterTrait;
use Hub2\Traits\Hub2SetterTrait;

class Hub2Gateway extends Hub2Abstract
{
    use Hub2GetterTrait, Hub2SetterTrait, Hub2GatewayHelperTrait;

    /**
     * Hub2Gateway constructor.
     *
     * @param int $merchantID
     * @param string $apiKey
     * @param bool $sandbox
     */
    public function __construct(int $merchantID = null, string $apiKey = null, bool $sandbox = null)
    {
        if (!is_null($merchantID)) {
            static::$merchantID = $merchantID;
            static::$apiKey = $apiKey;
            static::$sandbox = $sandbox;
        }
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exceptions\Hub2Exception
     */
    public static function openTransaction(array $data)
    {
        // Check data if valid
        Hub2ValidatorHelper::validateOpenTransaction($data);

        // Post data content
        $postData = array_merge(
            $data,
            self::getRequestConfig()
        );

        /**
         * Return request response
         */
        return Hub2RequestHelper::setOpenTransactionRequest($postData);
    }

    /**
     * @param string $tuid
     * @return mixed
     * @throws Exceptions\Hub2Exception
     */
    public static function checkTransaction(string $tuid)
    {
        // Check $tuid is valid
        Hub2ValidatorHelper::validateTuid($tuid);

        /**
         * Return request response
         */
        return Hub2RequestHelper::getTransactionStatusRequest(
            hub2_filter_string($tuid)
        );
    }

    /**
     * Cancel transaction
     *
     * @param string $tuid
     * @return mixed
     * @throws Exceptions\Hub2Exception
     */
    public static function cancelTransaction(string $tuid)
    {
        // Check $tuid is valid
        Hub2ValidatorHelper::validateTuid($tuid);

        /**
         * Return request response
         */
        return Hub2RequestHelper::setCancelTransactionRequest(
            hub2_filter_string($tuid),
            self::getRequestConfig()
        );
    }

    /**
     * Open Transaction
     *
     * @param array $data
     * @return mixed
     * @throws Exceptions\Hub2Exception
     */
    public static function prepareTransaction(array $data): array
    {
        // Check prepare transaction
        Hub2ValidatorHelper::validatePrepareTransaction($data);

        /**
         * Return PostData
         */
        return [
            "customer_id" => (string) hub2_filter_string($data['customer_id']),
            "purchase_ref" => (string) hub2_filter_string($data['purchase_ref']),
            "in" => [
                "method" => (string) hub2_filter_string($data['method']),
                "amount" => (float) hub2_filter_float($data['amount']),
                "currency" => (string) hub2_filter_string($data['currency'])
            ]
        ];
    }
}
