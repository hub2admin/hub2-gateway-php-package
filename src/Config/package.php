<?php

return [
    /**
     * API URLs
     */
    'api' => [
        'preprod' => 'https://sasha.api.hub2.io/v1/',
        'prod' => 'https://api.hub2.io/v1/',
    ],

    /**
     * JS URLs
     */
    'js' => [
        'preprod' => 'https://aicha.js.hub2.io/v1/api.js',
        'prod' => 'https://js.hub2.io/v1/api.js',

        /**
         * Jquery plugin
         */
        'jquery' => [
            'redirect' => 'https://js.hub2.io/external/jquery-redirect.js',
        ]
    ],

    /**
     * CSS URLs
     */
    'css' => [
        'preprod' => 'https://aicha.static.hub2.io/v1/api.css',
        'prod' => 'https://static.hub2.io/v1/api.css',
    ],

    /**
     * HUB2 PREFIX
     */
    'prefix' => 'hub2_',

    /**
     * HUB2 DEFAULT
     */
    'default' => [
        /**
         * HUB2 CURRENCIES
         */
        'currencies' => [
            'XOF', 'XAF', 'OUV', 'EUR'
        ],

        /**
         * HUB2 METHODS
         */
        'methods' => [
            'mb'
        ],

        /**
         * HUB2 FIELDS
         */
        'fields' => [
            'customer_id', 'purchase_ref', 'amount', 'method', 'currency'
        ],
    ],
];


