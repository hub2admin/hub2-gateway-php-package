<?php

namespace Hub2\Helpers;

use Hub2\Exceptions\Hub2Exception;

class Hub2ValidatorHelper
{
    /**
     * validate tuid
     * @param string $tuid
     * @throws Hub2Exception
     */
    public static function validateTuid(string $tuid)
    {
        if (!is_string($tuid)) {
            throw new Hub2Exception('tuid is a string');
        }

        if (is_null($tuid) || empty($tuid)) {
            throw new Hub2Exception('tuid is required');
        }
    }

    /**
     * @param array $data
     * @throws Hub2Exception
     */
    public static function validateSetConfig(array $data)
    {
        if (!is_array($data)) {
            throw new Hub2Exception('the parameter is array');
        }

        if (!array_key_exists('merchant_id', $data)) {
            throw new Hub2Exception('the merchant_id is required');
        }

        if (!array_key_exists('api_key', $data)) {
            throw new Hub2Exception('the api_key is required');
        }
    }

    /**
     * validate token
     * @param string $token
     * @throws Hub2Exception
     */
    public static function validateToken(string $token)
    {
        if (!is_string($token)) {
            throw new Hub2Exception('token is a string');
        }

        if (is_null($token) || empty($token)) {
            throw new Hub2Exception('token is required');
        }
    }

    /**
     * Validate open transaction data
     * @param array $data
     * @throws Hub2Exception
     */
    public static function validateOpenTransaction(array $data)
    {
        if (!is_array($data)) {
            throw new Hub2Exception('the parameter is array');
        }

        if (!array_key_exists('customer_id', $data)) {
            throw new Hub2Exception('customer_id is required');
        }

        if (!array_key_exists('purchase_ref', $data)) {
            throw new Hub2Exception('purchase_ref is required');
        }

        if (!array_key_exists('in', $data)) {
            throw new Hub2Exception('in is required');
        }

        if (!array_key_exists('method', $data['in'])) {
            throw new Hub2Exception('method is required');
        }

        if (!array_key_exists('amount', $data['in'])) {
            throw new Hub2Exception('amount is required');
        }

        if (!array_key_exists('currency', $data['in'])) {
            throw new Hub2Exception('currency is required');
        }

        if (!in_array($data['in']['currency'], hub2_configs('default.currencies'))) {
            throw new Hub2Exception('this currency does\'t exist');
        }

        if (!in_array($data['in']['method'], hub2_configs('default.methods'))) {
            throw new Hub2Exception('this method does\'t exist');
        }

        if (!is_string($data['purchase_ref'])) {
            throw new Hub2Exception('purchase_ref is string');
        }

        if (!is_string($data['customer_id'])) {
            throw new Hub2Exception('customer_id is string');
        }

        if (!is_float($data['in']['amount']) && !is_double($data['in']['amount']) && !is_integer($data['in']['amount'])) {
            throw new Hub2Exception('amount is float, double or integer');
        }
    }

    /**
     * Validate prepare transaction
     *
     * @param array $data
     * @throws Hub2Exception
     */
    public static function validatePrepareTransaction(array $data)
    {
        if (!is_array($data)) {
            throw new Hub2Exception('the parameter is array');
        }

        if (!array_key_exists('customer_id', $data)) {
            throw new Hub2Exception('customer_id is required');
        }

        if (!array_key_exists('purchase_ref', $data)) {
            throw new Hub2Exception('purchase_ref is required');
        }

        if (!array_key_exists('method', $data)) {
            throw new Hub2Exception('method is required');
        }

        if (!array_key_exists('amount', $data)) {
            throw new Hub2Exception('amount is required');
        }

        if (!array_key_exists('currency', $data)) {
            throw new Hub2Exception('currency is required');
        }

        if (!in_array($data['currency'], hub2_configs('default.currencies'))) {
            throw new Hub2Exception('this currency does\'t exist');
        }

        if (!in_array($data['method'], hub2_configs('default.methods'))) {
            throw new Hub2Exception('this method does\'t exist');
        }

        if (!is_string($data['purchase_ref'])) {
            throw new Hub2Exception('purchase_ref is string');
        }

        if (!is_string($data['customer_id'])) {
            throw new Hub2Exception('customer_id is string');
        }

        if (!is_float((float) $data['amount']) && !is_double((float) $data['amount']) && !is_integer((float) $data['amount'])) {
            throw new Hub2Exception('amount is float, double or integer');
        }
    }
}