<?php

namespace Hub2\Helpers;

use GuzzleHttp\Client;
use Hub2\Abstracts\Hub2RequestAbstract;
use Hub2\Hub2Gateway;

class Hub2RequestHelper extends Hub2RequestAbstract
{
    /**
     * @var array
     */
    private static $headers = [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ];

    /**
     * Hub2RequestHelper constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * @param array $data
     * @return string
     */
    public static function setOpenTransactionRequest(array $data)
    {
        self::injectHeader();

        self::$client = new Client();

        $res = self::$client->request(
            'POST',
            self::prefixURL() .'transactions',
            [
                'json' => $data,
                'headers' => self::$headers
            ]
        );

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @param string $tuid
     * @return \Psr\Http\Message\StreamInterface
     */
    public static function getTransactionStatusRequest(string $tuid)
    {
        self::injectHeader();

        self::$client = new Client();

        $res = self::$client->request(
            'GET',
            self::prefixURL() .'transactions/'. $tuid .'/status/in',
            [
                'headers' => self::$headers
            ]
        );

        return json_decode($res->getBody()->getContents());
    }

    /**
     * Cancel transaction Request
     *
     * @param string $tuid
     * @param array $data
     * @return \Psr\Http\Message\StreamInterface
     */
    public static function setCancelTransactionRequest(string $tuid, array $data)
    {
        self::injectHeader();

        self::$client = new Client();

        $res = self::$client->request(
            'PUT',
            self::prefixURL() .'transactions/'. $tuid .'/cancel',
            [
                'json' => $data,
                'headers' => self::$headers
            ]
        );

        return json_decode($res->getBody()->getContents());
    }

    /**
     * Inject H2 sandbox header
     */
    public static function injectHeader()
    {
        if (Hub2Gateway::getSandbox()) {
            self::$headers = array_merge(
                self::$headers, [
                'H2-Sandbox' => true
            ]);
        }
    }

    /**
     * API URLS
     * @return mixed
     */
    public static function prefixURL()
    {
        return Hub2Gateway::getSandbox() ?
            hub2_configs('api.preprod') :
            hub2_configs('api.prod');
    }
}