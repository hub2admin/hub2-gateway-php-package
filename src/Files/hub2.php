<?php
/**
 * Configs
 *
 * @param $key
 * @return mixed
 */
function hub2_configs($key)
{
    $configs = include(__DIR__ .'/../Config/package.php');

    return array_get($configs, $key);
}

/**
 * Filter var helper
 * @param $value
 * @return mixed
 */
function hub2_filter_string($value) {
    return filter_var($value, FILTER_SANITIZE_STRING);
}

/**
 * Filter var helper
 * @param $value
 * @return mixed
 */
function hub2_filter_float($value) {
    return filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT);
}
