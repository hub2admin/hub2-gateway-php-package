<?php

namespace Hub2\Traits;

use Hub2\Helpers\Hub2ValidatorHelper;

trait Hub2SetterTrait
{
    /**
     * Set Global config
     *
     * @param array $config
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public static function setConfig(array $config)
    {
        Hub2ValidatorHelper::validateSetConfig($config);

        self::$merchantID = (int) $config['merchant_id'] ?? null;
        self::$apiKey = hub2_filter_string((string) $config['api_key']) ?? null;
        self::$sandbox = isset($config['sandbox']) ? (bool) $config['sandbox'] : true;
    }

    /**
     * Set Merchant ID
     *
     * @param int $merchantID
     */
    public static function setMerchantID(int $merchantID)
    {
        self::$merchantID = (int) $merchantID ?? null;
    }

    /**
     * Set API Key
     *
     * @param string $apiKey
     */
    public static function setApiKey(string $apiKey)
    {
        self::$apiKey = (string) $apiKey ?? null;
    }

    /**
     * Set Test Mode
     *
     * @param bool $sandbox
     */
    public static function setSandbox(bool $sandbox)
    {
        self::$sandbox = (bool) $sandbox ?? true;
    }
}
