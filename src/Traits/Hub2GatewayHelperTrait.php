<?php

namespace Hub2\Traits;

use Hub2\Exceptions\Hub2Exception;
use Hub2\Helpers\Hub2ValidatorHelper;

trait Hub2GatewayHelperTrait
{
    /**
     * Filled content
     *
     * @param array $data
     * @return array
     * @throws Hub2Exception
     */
    public static function fillContent(array $data)
    {
        if (!is_array($data)) {
            throw new Hub2Exception('the parameter is array');
        }

        return array_filter($data, function($key) {
            return in_array($key, hub2_configs('default.fields'));
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Hub2 script init
     *
     * @param string $token
     * @param string $tuid
     * @throws Hub2Exception
     */
    public static function scriptInit(string $token = null, string $tuid = null)
    {
        if (!is_null($token) && !is_null($tuid)) {
            /** filter @var $token */
            $token = hub2_filter_string($token);

            /** filter @var $tuid */
            $tuid = hub2_filter_string($tuid);

            /**
             * Validator $token & $tuid
             */
            Hub2ValidatorHelper::validateToken($token);
            Hub2ValidatorHelper::validateTuid($tuid);

            /**
             * Inject js Value
             */
            if (self::getSandbox()) {
                echo '<script src="'. hub2_configs('js.preprod') .'"></script>';
            } else {
                echo '<script src="'. hub2_configs('js.prod') .'"></script>';
            }

            echo '<script src="'. hub2_configs('js.jquery.redirect') .'"></script>';
            echo '<script>
                var hub2 = new Hub2("'. hub2_filter_string($tuid) .'", "'. hub2_filter_string($token) .'");
                hub2.init();
             </script>';
        }
    }

    /**
     * Hub2 css init
     *
     * @param string $token
     * @param string $tuid
     * @throws Hub2Exception
     */
    public static function cssInit(string $token = null, string $tuid = null)
    {
        if (!is_null($token) && !is_null($tuid)) {
            /** filter @var $token */
            $token = hub2_filter_string($token);

            /** filter @var $tuid */
            $tuid = hub2_filter_string($tuid);

            /**
             * Validator $token & $tuid
             */
            Hub2ValidatorHelper::validateToken($token);
            Hub2ValidatorHelper::validateTuid($tuid);

            /**
             * Inject css Value
             */
            if (self::getSandbox()) {
                echo '<link rel="stylesheet" href="'. hub2_configs('css.preprod') .'" />';
            } else {
                echo '<link rel="stylesheet" href="'. hub2_configs('css.prod') .'" />';
            }
        }
    }
}
