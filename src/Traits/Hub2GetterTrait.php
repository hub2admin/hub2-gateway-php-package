<?php

namespace Hub2\Traits;

trait Hub2GetterTrait
{
    /**
     * Get Global config
     *
     * @return array
     */
    public static function getConfig()
    {
        return [
            'merchant_id' => self::$merchantID,
            'api_key' => self::$apiKey,
            'sandbox' => self::$sandbox
        ];
    }

    /**
     * Request method
     * @return array
     */
    public static function getRequestConfig()
    {
        return [
            'merchant_id' => self::$merchantID,
            'api_key' => self::$apiKey
        ];
    }

    /**
     * Get Merchant ID
     */
    public static function getMerchantID()
    {
        return self::$merchantID;
    }

    /**
     * Get API Key
     */
    public static function getApiKey()
    {
        return self::$apiKey;
    }

    /**
     * Get Test Mode
     */
    public static function getSandbox()
    {
        return self::$sandbox;
    }
}
