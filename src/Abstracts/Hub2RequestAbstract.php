<?php

namespace Hub2\Abstracts;

use GuzzleHttp\Client;

abstract class Hub2RequestAbstract
{
    /**
     * @var Client
     */
    public static $client;

    /**
     * Hub2RequestAbstract constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        self::$client = $client;
    }
}