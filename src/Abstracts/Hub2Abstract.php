<?php

namespace Hub2\Abstracts;

abstract class Hub2Abstract
{
    /**
     * @var null
     */
    protected static $merchantID = null;

    /**
     * @var null
     */
    protected static $apiKey = null;

    /**
     * @var bool
     */
    protected static $sandbox = true;

    /**
     * Open Transaction
     *
     * @param array $data
     * @return mixed
     */
    abstract public static function openTransaction(array $data);

    /**
     * Check Transaction
     *
     * @param $tuid
     * @return mixed
     */
    abstract public static function checkTransaction(string $tuid);

    /**
     * Cancel transaction
     *
     * @param $tuid
     * @return mixed
     */
    abstract public static function cancelTransaction(string $tuid);

    /**
     * Open Transaction
     *
     * @param array $data
     * @return mixed
     */
    abstract public static function prepareTransaction(array $data): array;
}
