<?php

namespace Tests\Unit;

use Hub2\Hub2Gateway;
use PHPUnit\Framework\TestCase;

class GetterAndSetterTest extends TestCase
{
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        Hub2Gateway::setConfig([
            'merchant_id' => 1,
            'api_key' => 'hello'
        ]);
    }

    /**
     * Test get config
     */
    public function testGetConfig()
    {
        $config = [
            'merchant_id' => 1,
            'api_key' => 'hello',
            'sandbox' => true
        ];

        $this->assertEquals(Hub2Gateway::getConfig(), $config);
    }

    /**
     * Get merchant id
     */
    public function test_get_merchant_id()
    {
        $this->assertEquals(Hub2Gateway::getMerchantID(), 1);
    }

    /**
     * Get api key
     */
    public function test_get_api_key()
    {
        $this->assertEquals(Hub2Gateway::getApiKey(), 'hello');
    }

    /**
     * Get sandbox
     */
    public function test_get_sandbox()
    {
        $this->assertTrue(Hub2Gateway::getSandbox());
    }

    /**
     * Set merchant id
     */
    public function test_set_merchant_id()
    {
        Hub2Gateway::setMerchantID(10);

        $this->assertEquals(Hub2Gateway::getMerchantID(), 10);
    }

    /**
     * Set api key
     */
    public function test_set_api_key()
    {
        Hub2Gateway::setApiKey('bonsoir');

        $this->assertEquals(Hub2Gateway::getApiKey(), 'bonsoir');
    }

    /**
     * Set sandbox
     */
    public function test_set_sandbox()
    {
        Hub2Gateway::setSandbox(false);

        $this->assertFalse(Hub2Gateway::getSandbox());
    }

    /**
     * Test set and get config
     */
    public function testSetAndGetConfig()
    {
        $config = [
            'merchant_id' => 2,
            'api_key' => 'hello',
            'sandbox' => true
        ];

        Hub2Gateway::setConfig($config);

        $this->assertEquals(Hub2Gateway::getConfig(), $config);
    }
}
