<?php

namespace Tests\Unit;

use Hub2\Hub2Gateway;
use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase
{
    /**
     * @var array
     */
    protected $data = [
        'shop' => 'Demo Shop',
        'amount' => 20,
        'customer_id' => '1',
        'currency' => 'XOF',
        'method' => 'mb',
        'purchase_ref' => 'DEMO234590L',
        'country' => 'CI',
    ];

    /**
     * Filled content test
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public function test_fill_content()
    {
        $postData = Hub2Gateway::fillContent($this->data);

        $this->assertEquals($postData, [
            'amount' => 20,
            'customer_id' => 1,
            'currency' => 'XOF',
            'method' => 'mb',
            'purchase_ref' => 'DEMO234590L',
        ]);
    }

    /**
     * Prepare transaction test
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public function test_prepare_transaction()
    {
        $postData = Hub2Gateway::fillContent($this->data);

        $content = Hub2Gateway::prepareTransaction($postData);

        $this->assertEquals($content, [
            'customer_id' => 1,
            'in' => [
                'amount' => 20,
                'method' => 'mb',
                'currency' => 'XOF',
            ],
            'purchase_ref' => 'DEMO234590L',
        ]);
    }

    /**
     * Open transaction
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public function test_open_transaction()
    {
        $this->setConfiguration();

        $request = $this->openTransaction();

        $this->assertEquals(3, count((array) $request));
        $this->assertTrue($request->success);
        $this->assertArrayHasKey('token', (array) $request);
        $this->assertArrayHasKey('tuid', (array) $request);
        $this->assertArrayHasKey('success', (array) $request);
    }

    /**
     * Check transaction test
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public function test_check_transaction()
    {
        $this->setConfiguration();
        $openRequest = $this->openTransaction();
        $tuid = $openRequest->tuid;

        $request = Hub2Gateway::checkTransaction($tuid);

        $this->assertEquals(6, count((array) $request));
        $this->assertArrayHasKey('status', (array) $request);
        $this->assertArrayHasKey('tuid', (array) $request);
        $this->assertArrayHasKey('updated_at', (array) $request);
        $this->assertArrayHasKey('purchase_ref', (array) $request);
        $this->assertArrayHasKey('action', (array) $request);
        $this->assertArrayHasKey('success', (array) $request);

        $this->assertEquals(3, count((array) $request->status));
        $this->assertArrayHasKey('status', (array) $request->status);
        $this->assertArrayHasKey('code', (array) $request->status);
        $this->assertArrayHasKey('msg', (array) $request->status);

        $this->assertFalse($request->action);
        $this->assertTrue($request->success);
    }

    /**
     * Cancel transaction
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public function test_cancel_transaction()
    {
        $this->setConfiguration();
        $openRequest = $this->openTransaction();
        $tuid = $openRequest->tuid;

        $request = Hub2Gateway::cancelTransaction($tuid);

        $this->assertTrue($request->success);
        $this->assertEquals(2, count((array) $request));
        $this->assertArrayHasKey('success', (array) $request);
        $this->assertArrayHasKey('status', (array) $request);
    }

    /**
     * Open Transaction
     *
     * @return mixed
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public function openTransaction()
    {
        return Hub2Gateway::openTransaction([
            'customer_id' => '1',
            'in' => [
                'amount' => 20,
                'method' => 'mb',
                'currency' => 'XOF',
            ],
            'purchase_ref' => 'DEMO234590L',
        ]);
    }

    /**
     * Set config
     * @throws \Hub2\Exceptions\Hub2Exception
     */
    public function setConfiguration()
    {
        Hub2Gateway::setConfig([
            'merchant_id' => $_ENV['MERCHANT_ID'],
            'api_key' => $_ENV['API_KEY'],
            'sandbox' => true
        ]);
    }
}
